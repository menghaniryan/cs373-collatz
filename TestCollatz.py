#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import (
    collatz_read,
    collatz_eval,
    collatz_print,
    collatz_solve,
    roundup,
    collatz_helper,
    collatz_recursive,
)

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(372010, 867111)
        self.assertEqual(v, 525)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    # ----
    # collatz_helper
    # ----

    def test_helper_1(self):
        v = collatz_helper(10, 20)
        self.assertEqual(v, 21)

    def test_helper_2(self):
        v = collatz_helper(400422, 717857)
        self.assertEqual(v, 509)

    def test_helper_3(self):
        v = collatz_helper(290260, 629790)
        self.assertEqual(v, 509)

    # ----
    # collatz_recursive
    # ----

    def test_recursive_1(self):
        v = collatz_recursive(11)
        self.assertEqual(v, 15)

    def test_recursive_2(self):
        v = collatz_recursive(88453)
        self.assertEqual(v, 134)

    # -----
    # roundup
    # -----

    def test_roundup(self):
        v = roundup(1)
        self.assertEqual(v, 1000)

    def test_roundup_2(self):
        v = roundup(1211)
        self.assertEqual(v, 2000)

    def test_roundup_3(self):
        v = roundup(2000)
        self.assertEqual(v, 2000)

    def test_roundup_4(self):
        v = roundup(868380)
        self.assertEqual(v, 869000)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 23141234, 12341234123, 1239817239487129348)
        self.assertEqual(w.getvalue(), "23141234 12341234123 1239817239487129348\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )

    def test_solve_2(self):
        r = StringIO("30 85\n101 501\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "30 85 116\n101 501 144\n")

    def test_solve_3(self):
        r = StringIO("274269 79860\n213705 174466\n926673 192217\n711632 599962\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "274269 79860 443\n213705 174466 373\n926673 192217 525\n711632 599962 509\n",
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
