# CS373: Software Engineering Collatz Repo

* Name: Ryan Menghani

* EID: rvm447

* GitLab ID: menghaniryan

* HackerRank ID: menghaniryan

* Git SHA: 53c890b71fec1d7e9084f821146f2d2bcfc2f5b3

* GitLab Pipelines: https://gitlab.com/menghaniryan/cs373-collatz/pipelines

* Estimated completion time: 10 hours

* Actual completion time: 12 hours

* Comments: 
Most of the time will probably be spent understanding the workflow.
Added pylint: disable = too-many-lines in Collatz.py because the metacache makes the Collatz.py too long for pylint

